setwd('D:\\Google Drive\\Thesis\\Project')
#Remove existing variables and garbage collect them
rm(list = ls())
gc()
#Function to check if package in already installed or not
checkPackage <- function(package) {
	if (!package %in% installed.packages()) install.packages(package)
}
#Learning data
analysisData <- read.table('ClassifierAnalysisSet0.txt', header=T, sep='\t')
analysisData[,3] = analysisData[,3]*0.5
analysisData$Class = factor(analysisData$Class)
dim(analysisData)
str(analysisData) #check data
head(analysisData) 
summary = summary(analysisData[, 1:11]) #summarize data. Gives mean or factor count
write.csv(summary , "graph\\DataSummary.csv")

#Hmisc package to describe about each attributes. Important information like missing data, unique data, mean, and frequency can be obtained
checkPackage("Hmisc")
library(Hmisc)
describe(analysisData) # demographics

#create graph directory
if (!file.exists('graph')){    
    dir.create('graph')
}

#Graph to show the % of responses in learning data
setEPS()
postscript("graph\\ResponseChart.eps", width=6.8, height=6)
#pdf("graph\\ResponseChart.pdf", width=6.8, height=5)
#Get response %
percent.detection.responded <- round(100 * prop.table(table(analysisData$Class)), digits=2)
chartLabels <- paste(c('Smartphone not Found\n', 'Smartphone Present\n'), percent.detection.responded, "%", sep="")
# draw pie chart
pie(percent.detection.responded, labels=chartLabels, main="Distribution of observations")
dev.off()
rm(percent.detection.responded, chartLabels)

#correlation diagram
checkPackage("corrgram")
checkPackage("ellipse")
library(corrgram)
library(ellipse)
correlationMatrix <- cor(analysisData[,which(sapply(analysisData, is.numeric))])
write.csv(round(correlationMatrix, 3), "graph\\CorrelationMatrix.csv")
setEPS()
postscript("graph\\Correlation.eps", width=6.8, height=9)
#pdf("graph\\Correlation.pdf", width=6.8, height=9)
corrgram(correlationMatrix, order = TRUE, lower.panel = panel.shade, upper.panel = panel.pie, text.panel = panel.txt, main = "Correlation Between Variables")
dev.off()

#Feature selection by removing redundant features 
checkPackage("caret")
library(caret)
removedFeatures = vector(mode="list", length=2)
removedFeatures[[1]] = findCorrelation(cor(analysisData[,-c(12)]), cutoff = .70, verbose = FALSE)
#[1] 1 8 6 9 7
#correlation diagram after removal
correlationMatrix <- cor(analysisData[,-c(removedFeatures[[1]], 12)])
write.csv(round(correlationMatrix, 3), "graph\\CorrelationMatrixAfterRemoval1.csv")
setEPS()
postscript("graph\\CorrelationAfterRemoval1.eps", width=6.8, height=9)
#pdf('graph\\CorrelationAfterRemoval1.pdf', width=12, height=9, paper="a4", pointsize=6)
corrgram(correlationMatrix, order = TRUE, lower.panel = panel.shade, upper.panel = panel.pie, text.panel = panel.txt, main = "Correlation Between Remaining Variables")
dev.off()

#Feature selection by Recursive Feature Elimination or RFE
set.seed(7)
checkPackage("mlbench")
checkPackage("randomForest")
checkPackage("e1071")
library(mlbench)
library(randomForest)
# define the control using a randomForest
control <- rfeControl(functions=rfFuncs, method="cv", number=10)
# run the RFE algorithm
results <- rfe(analysisData[,1:11], analysisData[,12], sizes=c(1:11), rfeControl=control)
# list the chosen features
selectedVar = predictors(results)
selectedVar = c(selectedVar, "Class")
removedFeatures[[2]] =  setdiff(names(analysisData), selectedVar)
# plot the results
setEPS()
postscript("graph\\variableSelectByRFE.eps", width=6.8, height=9)
#pdf('graph\\variableSelectByRFE.pdf')
plot(results, type=c("g", "o"))
dev.off()

#######Perform Cross-validation for each of the variable selection conditions
checkPackage("ROCR")
checkPackage("boot")
library(ROCR)
library(boot)
library(MASS)

#Randomly shuffle the data
analysisData<-analysisData[sample(nrow(analysisData)),]
#Create 10 equally size folds
folds <- cut(seq(1,nrow(analysisData)),breaks=10,labels=FALSE)
#Copy data
MainData = analysisData

DataErrors = matrix(NA, nrow = 3, ncol = 3)
DataAreaOfCurves = matrix(NA, nrow = 3, ncol = 3)
DataAccuracy = matrix(NA, nrow = 3, ncol = 3)
DataErrorsSD = matrix(NA, nrow = 3, ncol = 3)
DataAreaOfCurvesSD = matrix(NA, nrow = 3, ncol = 3)
DataAccuracySD = matrix(NA, nrow = 3, ncol = 3)
DataCutoff = matrix(NA, nrow = 3, ncol = 3)
DataCutoffSD = matrix(NA, nrow = 3, ncol = 3)
DataSensitivity= matrix(NA, nrow = 3, ncol = 3)
DataSensitivitySD = matrix(NA, nrow = 3, ncol = 3)
DataSpecificity= matrix(NA, nrow = 3, ncol = 3)
DataSpecificitySD = matrix(NA, nrow = 3, ncol = 3)

#function to calculate optimal cutoff where both sensitivity and specificity are weighed equally
opt.cut = function(perf, pred){
    cut.ind = mapply(FUN=function(x, y, p){
        d = (x - 0)^2 + (y-1)^2
        ind = which(d == min(d))
        c(sensitivity = y[[ind]], specificity = 1-x[[ind]], 
            cutoff = p[[ind]])
    }, perf@x.values, perf@y.values, pred@cutoffs)
}

for(j in 1:3) {
	#Perform 10 fold cross validation
	errors = matrix(NA, nrow = 10, ncol = 3)
	preditionsLR = vector(mode="list", length=10)
	preditionsLDA = vector(mode="list", length=10)
	preditionsQDA = vector(mode="list", length=10)
	testClassLabels = vector(mode="list", length=10)
	areaOfCurves = matrix(NA, nrow = 10, ncol = 3)
	accuracys = matrix(NA, nrow = 10, ncol = 3)

	Sensitivity = matrix(NA, nrow = 10, ncol = 3)
	SensitivitySD = matrix(NA, nrow = 10, ncol = 3)
	Specificity = matrix(NA, nrow = 10, ncol = 3)
	SpecificitySD = matrix(NA, nrow = 10, ncol = 3)
	Cutoff = matrix(NA, nrow = 10, ncol = 3)
	CutoffSD = matrix(NA, nrow = 10, ncol = 3)
	
	if(j==3) {
		analysisData = MainData
	} else if(j!=2) {
		analysisData = MainData[, -removedFeatures[[j]]]
	} else {
		analysisData = MainData[, !(names(MainData) %in% removedFeatures[[j]])]
	}
	for(i in 1:10){
		#Segement your data by fold using the which() function 
		testIndexes <- which(folds==i,arr.ind=TRUE)
		testData <- analysisData[testIndexes, ]
		trainData <- analysisData[-testIndexes, ]
		#Use the test and train data partitions 
		#LR
		glm.fit <- glm(Class ~., data = trainData, family = binomial)
		glm.probs <- predict(glm.fit, testData[, !(names(analysisData) %in% c("Class"))], type = "response")
		glm.pred=rep(0,dim(testData)[1])
		glm.pred[glm.probs >.5]=1
		table(glm.pred ,testData$Class)	
		error = mean(glm.pred!=testData$Class)
		errors[i, 1] = error
		
		#LDA
		lda.fit=lda(Class~., data=trainData)
		lda.pred=predict(lda.fit, testData[, !(names(analysisData) %in% c("Class"))])
		lda.class=lda.pred$class
		table(lda.class ,testData$Class)
		error = mean(lda.class!=testData$Class)
		errors[i, 2] = error
		
		#QDA
		qda.fit=qda(Class~., data=trainData)
		qda.pred=predict(qda.fit, testData[, !(names(analysisData) %in% c("Class"))])
		qda.class=qda.pred$class
		table(qda.class ,testData$Class)
		error = mean(qda.class!=testData$Class)
		errors[i, 3] = error	

		predLR = prediction( predictions = glm.probs, labels = testData$Class)	
		perfLR <-  performance(predLR, "spec", "sens")
		
		predLDA = prediction( predictions = lda.pred$posterior[,"1"], labels = testData$Class)	
		perfLDA <-  performance(predLDA, "spec", "sens")
		
		predQDA = prediction( predictions = qda.pred$posterior[,"1"], labels = testData$Class)	
		perfQDA <-  performance(predQDA, "spec", "sens")	
		
		#performance measures
		testClassLabels[[i]] = testData$Class
		preditionsLR[[i]] = glm.probs
		areaOfCurves[i, 1] = performance(predLR, measure = "auc")@y.values[[1]]
		preditionsLDA[[i]] = lda.pred$posterior[,"1"]
		areaOfCurves[i, 2] = performance(predLDA, measure = "auc")@y.values[[1]]
		preditionsQDA[[i]] = qda.pred$posterior[,"1"]
		areaOfCurves[i, 3] = performance(predQDA, measure = "auc")@y.values[[1]]
		
		perfLR1 <-  performance(predLR, measure = "tpr", x.measure = "fpr")
		perfLDA1 <-  performance(predLDA, measure = "tpr", x.measure = "fpr")		
		perfQDA1 <-  performance(predQDA, measure = "tpr", x.measure = "fpr")
		
		valuesLR = opt.cut(perfLR1, predLR)
		valuesLDA = opt.cut(perfLDA1, predLDA)
		valuesQDA = opt.cut(perfQDA1, predQDA)
	
		Sensitivity[i, 1] = valuesLR[1]		
		Specificity[i, 1] = valuesLR[2]		
		Cutoff[i, 1] = valuesLR[3]		
		acc.perf = performance(predLR, measure = "acc")
		list.acc <- unlist(acc.perf@y.values[[1]])
		accuracys[i, 1] = list.acc[which(acc.perf@x.values[[1]] == Cutoff[i, 1])]
		
		Sensitivity[i, 2] = valuesLDA[1]		
		Specificity[i, 2] = valuesLDA[2]		
		Cutoff[i, 2] = valuesLDA[3]		
		acc.perf = performance(predLDA, measure = "acc")
		list.acc <- unlist(acc.perf@y.values[[1]])
		accuracys[i, 2] = list.acc[which(acc.perf@x.values[[1]] == Cutoff[i, 2])]
		
		Sensitivity[i, 3] = valuesQDA[1]
		SensitivitySD[i, 3] = sd(valuesQDA[1])
		Specificity[i, 3] = valuesQDA[2]
		SpecificitySD[i, 3] = sd(valuesQDA[2])
		Cutoff[i, 3] = valuesQDA[3]
		CutoffSD[i, 3] = sd(valuesQDA[3])
		acc.perf = performance(predQDA, measure = "acc")
		list.acc <- unlist(acc.perf@y.values[[1]])
		accuracys[i, 3] = list.acc[which(acc.perf@x.values[[1]] == Cutoff[i, 3])]
		
		#par(mfrow = c(2,2))
		#Recall vs FPR - high recall at low FPR
		setEPS()
		postscript(paste(c("graph\\CrossComparisionRoc", j, i ,".eps"), collapse=""), width=6.8, height=9)
		plot(unlist(attr(perfLR, "x.values")), x= 1-unlist(attr(perfLR, "y.values")),  xlab= "False positive rate", ylab= "Recall", type = "l", lwd = 2)
		lines(y = unlist(attr(perfLDA, "x.values")), x= 1-unlist(attr(perfLDA, "y.values")),  col = "magenta", lty = "dashed", lwd = 2)
		lines(y = unlist(attr(perfQDA, "x.values")), x= 1-unlist(attr(perfQDA, "y.values")),  col = "blue", lwd = 2, lty = "dotdash")	
		legend(0.5,0.2, legend = c("LR", "LDA", "QDA"), fill = c("black", "magenta", "blue", "red"), bty = "n")
		dev.off();

		# the higher the probability the less FPR we want
		setEPS()
		postscript(paste(c("graph\\CrossComparisionFPR", j, i ,".eps"), collapse=""), width=6.8, height=9)
		plot(unlist(attr(perfLR,"alpha.values")), 1-unlist(attr(perfLR, "y.values")), xlab= "Probability", ylab= "False positive rate", type = "l", lwd = 2)
		lines(unlist(attr(perfLDA,"alpha.values")), 1-unlist(attr(perfLDA, "y.values")), col = "magenta", lty = "dashed", lwd = 2)
		lines(unlist(attr(perfQDA,"alpha.values")), 1-unlist(attr(perfQDA, "y.values")), col = "blue", lwd = 2, lty = "dotdash")	
		legend(0.5,1, legend = c("LR", "LDA", "QDA"), fill = c("black", "magenta", "blue", "red"), bty = "n")
		dev.off();

		# Higher recall at low probability
		setEPS()
		postscript(paste(c("graph\\CrossComparisionRecal", j, i ,".eps"), collapse=""), width=6.8, height=9)
		plot(unlist(attr(perfLR,"alpha.values")), unlist(attr(perfLR, "x.values")),xlab= "Probability", ylab= "Recall", type = "l", lwd = 2)
		lines(unlist(attr(perfLDA,"alpha.values")), unlist(attr(perfLDA, "x.values")), col = "magenta", lty = "dashed", lwd = 2)
		lines(unlist(attr(perfQDA,"alpha.values")), unlist(attr(perfQDA, "x.values")), col = "blue", lwd = 2, lty = "dotdash")	
		legend(0.1,0.3, legend = c("LR", "LDA", "QDA"), fill = c("black", "magenta", "blue", "red"), bty = "n")	
		#dev.copy2pdf(file=paste(c("graph\\CrossComparision", j, i ,".pdf"), collapse=""), width=6.8, height=9)
		dev.off();
	}

	#Avg performance measures of 10 folds
	DataErrors[j,] = colMeans(errors)
	DataErrorsSD[j,] = apply(errors, 2, sd)
	DataAreaOfCurves[j,] = colMeans(areaOfCurves)
	DataAreaOfCurvesSD[j,] = apply(areaOfCurves, 2, sd)
	DataAccuracy[j,] = colMeans(accuracys)
	DataAccuracySD[j,] = apply(accuracys, 2, sd)
	DataSensitivity[j,] = colMeans(Sensitivity)
	DataSensitivitySD[j,] = apply(Sensitivity, 2, sd)
	DataSpecificity[j,] = colMeans(Specificity)
	DataSpecificitySD[j,] = apply(Specificity, 2, sd)
	DataCutoff[j,] = colMeans(Cutoff)
	DataCutoffSD[j,] = apply(Cutoff, 2, sd)

	#Avg performance graph compairing the 3 classifiers
	predLR = prediction( predictions = preditionsLR, labels = testClassLabels)		
	predLDA = prediction( predictions = preditionsLDA, labels = testClassLabels)	
	predQDA = prediction( predictions = preditionsQDA, labels = testClassLabels)	

	perfLR <- performance(predLR, measure = "tpr", x.measure = "fpr")
	perfLDA <- performance(predLDA, measure = "tpr", x.measure = "fpr")
	perfQDA <- performance(predQDA, measure = "tpr", x.measure = "fpr")

	#if(j==3){
		setEPS()
		postscript(paste(c("graph\\AvgComparisonA", j ,".eps"), collapse=""), width=6.8, height=9)
	#}else{
	#pdf(paste(c("graph\\AvgComparisonA", j ,".pdf"), collapse=""), width=6.8, height=9)
	#}
	#ROC Curve
	#par(mfrow = c(2,2))
	plot(perfLR, lwd=2, avg="vertical", xlab="False positive rate", ylab="Recall", col="magenta")
	plot(perfLDA, lwd=2, avg="vertical", add=TRUE, col="blue", lty = "dashed")
	plot(perfQDA, lwd=2, avg="vertical", add=TRUE, col="red", lty = "dotdash")	
	legend(0.5,0.6, legend = c("LR", "LDA", "QDA"), col = c("magenta", "blue", "red"), lty=c("solid", "dashed", "dotdash"), lwd=c(2, 2, 2), bty = "n")
	dev.off();

	#Cutoff Vs FPR
	perfLR <-  performance(predLR, measure = "fpr", x.measure = "cutoff")
	perfLDA <-  performance(predLDA, measure = "fpr", x.measure = "cutoff")		
	perfQDA <-  performance(predQDA, measure = "fpr", x.measure = "cutoff")	
	
	#if(j==3){
		setEPS()
		postscript(paste(c("graph\\AvgComparisonB", j ,".eps"), collapse=""), width=6.8, height=9)
	#}else{
	#pdf(paste(c("graph\\AvgComparisonB", j ,".pdf"), collapse=""), width=6.8, height=9)
	#}

	plot(perfLR, lwd=2, avg="vertical", xlab= "Cutoff", ylab= "False positive rate", col="magenta")
	plot(perfLDA, lwd=2, avg="vertical", add=TRUE, col="blue", lty = "dashed")
	plot(perfQDA, lwd=2, avg="vertical", add=TRUE, col="red", lty = "dotdash")
	legend(0.5,0.9, legend = c("LR", "LDA", "QDA"), col = c("magenta", "blue", "red"), lty=c("solid", "dashed", "dotdash"), lwd=c(2, 2, 2),bty = "n")
	dev.off();

	#Cutoff Vs Recall
	perfLR <-  performance(predLR, measure = "tpr", x.measure = "cutoff")
	perfLDA <-  performance(predLDA, measure = "tpr", x.measure = "cutoff")		
	perfQDA <-  performance(predQDA, measure = "tpr", x.measure = "cutoff")
	
	#if(j==3){
		setEPS()
		postscript(paste(c("graph\\AvgComparisonC", j ,".eps"), collapse=""), width=6.8, height=9)
	#}else{
	#pdf(paste(c("graph\\AvgComparisonC", j ,".pdf"), collapse=""), width=6.8, height=9)
	#}

	plot(perfLR, lwd=2, avg="vertical", xlab= "Cutoff", ylab= "Recall", col="magenta")
	plot(perfLDA, lwd=2, avg="vertical", add=TRUE, col="blue", lty = "dashed")
	plot(perfQDA, lwd=2, avg="vertical", add=TRUE, col="red", lty = "dotdash")
	legend(0,0.6, legend = c("LR", "LDA", "QDA"), col = c("magenta", "blue", "red"), lty=c("solid", "dashed", "dotdash"), lwd=c(2, 2, 2),bty = "n")
	
	dev.off();
		
}

write.csv(DataAreaOfCurves, "graph\\DataAreaOfCurves.csv")
write.csv(DataAreaOfCurvesSD, "graph\\DataAreaOfCurvesSD.csv")
write.csv(DataAccuracy, "graph\\DataAccuracy.csv")
write.csv(DataAccuracySD, "graph\\DataAccuracySD.csv")
write.csv(DataCutoff, "graph\\Cutoff.csv")
write.csv(DataCutoffSD, "graph\\CutoffSD.csv")
write.csv(DataSensitivity, "graph\\Sensitivity.csv")
write.csv(DataSensitivitySD, "graph\\SensitivitySD.csv")
write.csv(DataSpecificity, "graph\\Specificity.csv")
write.csv(DataSpecificitySD, "graph\\SpecificitySD.csv")

#Train QDA for the dataset
qda.fit=qda(Class~., data=MainData)
qda.pred=predict(qda.fit, MainData[,-c(12)])
qda.pred1=rep(0,dim(MainData)[1])
qda.pred1[qda.pred$posterior[,"1"]>0.75]=1
table(qda.pred1,MainData$Class)
mean(qda.pred1==MainData$Class, na.rm = TRUE)
mean(qda.pred1!=MainData$Class, na.rm = TRUE)

#Get mean values
qda.fit

#Get covariance matirx
covarData0 <- MainData[MainData$Class==0, ]
covarianceMatrix0 <- cov(covarData0[, c(-12)])
det(covarianceMatrix0)
covarianceMatrix0Inv = solve(covarianceMatrix0)
write.csv(covarianceMatrix0, "graph\\covarianceMatrixClass0.csv")
write.csv(covarianceMatrix0Inv, "graph\\covarianceMatrixClass0Inv.csv")
covarData1 <- MainData[MainData$Class==1, ]
covarianceMatrix1 <- cov(covarData1[, -c(12)])
det(covarianceMatrix1)
write.csv(covarianceMatrix1, "graph\\covarianceMatrixClass1.csv")
covarianceMatrix1Inv = solve(covarianceMatrix1)
write.csv(covarianceMatrix1Inv, "graph\\covarianceMatrixClass1Inv.csv")

testData<- read.table('ESM_5.txt', header=T, sep='\t')
table(testData$Smartphone.Detector, testData$Class)
table(testData$Classifier, testData$Class)
tp = c(151, 143)
fp = c(59, 8)
tn = c(0, 44)
fn = c(0, 15)
data = data.frame(tp,tn,fn,fp)
row.names(data) = c('IBSD', 'SDSC')
colnames(data) = c('True Positive', 'True Negative', 'False Negative', 'False Positive')
par(mfrow = c(1,1))
setEPS()
postscript("graph\\compare.eps", width=6.8, height=6)
#pdf("graph\\compare.pdf", width=6.8, height=5)
par(oma = c(1, 1, 1, 1))
barplot(t(data/nrow(testData)*100),xlab="Detection Method", ylab="Percentage", col=c("magenta","blue", "grey", "red"), xlim=c(0, ncol(data)))
#mtext("Detection Method", outer=TRUE, side = 1, line=0, at=par("usr")[1]+0.2*diff(par("usr")[1:2]))
par(fig = c(0, 1, 0, 1), oma = c(0, 0, 0, 0), mar = c(0, 0, 0, 0), new = TRUE)
plot(0, 0, type = "n", bty = "n", xaxt = "n", yaxt = "n")
#barplot(t(data/nrow(testData)*100), main="IBSD Vs SDSC", xlab="Detection Method", ylab="Percentage", col=c("magenta","blue", "grey", "red"), xlim=c(0, ncol(data)))
L = legend(x=-0.8, y=1.1, colnames(data)[0:2], fill=c("magenta","blue"), horiz=TRUE, bty='n')
legend(x = L$rect$left, y = L$rect$top - 0.1,colnames(data)[3:4], fill=c("grey", "red"), horiz=TRUE, bty='n')
dev.off()